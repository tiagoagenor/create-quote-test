import { Component, OnInit } from '@angular/core';
import { WalletService } from '../wallet.service';
import { SnackbarService } from 'ngx-snackbar';

@Component({
  selector: 'app-funds',
  templateUrl: './funds.component.html',
  styleUrls: ['./funds.component.css']
})
export class FundsComponent implements OnInit {

  public priceAddFunds;
  public priceWithdrawFunds;

  constructor(
    private walletService: WalletService,
    private snackbarService: SnackbarService
  ) { }

  ngOnInit() {
  }

  addFunds() {
    const price = Number(this.priceAddFunds);
    if (price) {
      this.walletService.addFunds(price).subscribe(
        (success: any) => {
          this.priceAddFunds = '';
          this.walletService.eventWallet.emit(success.balance);
          this.snackbarService.add({
            msg: `Fundo adicionado com sucesso.`,
            action: {
              text: 'X',
            },
          });
        }
      );
    }
  }

  async withdrawFunds() {
    const price = Number(this.priceWithdrawFunds);
    if (price) {

      const result: any = await this.walletService.getWallet().toPromise();

      if (Number(result.balance) <= 0 || result.balance < price) {
        this.snackbarService.add({
          msg: `Desculpe saldo insuficiente.`,
          action: {
            text: 'X',
          },
        });
      } else {
        this.walletService.withdrawFunds(price).subscribe(
          (success: any) => {
            this.priceWithdrawFunds = '';
            this.walletService.eventWallet.emit(success.balance);

            this.snackbarService.add({
              msg: `Retirada foi feita com sucesso.`,
              action: {
                text: 'X',
              },
            });
          }
        );
      }


    }
  }

}
