import { Component, OnInit } from '@angular/core';
import { OrdersService } from '../orders.service';
import { WebSocketService } from '../web-socket.service';
import { WalletService } from '../wallet.service';
import { SnackbarService } from 'ngx-snackbar';

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.component.html',
  styleUrls: ['./wallet.component.css']
})
export class WalletComponent implements OnInit {

  public uniqueQuotes: any;
  public orders: any;
  constructor(
    private ordersService: OrdersService,
    private webSocketService: WebSocketService,
    private walletService: WalletService,
    private snackbarService: SnackbarService
  ) { }

  ngOnInit() {
    this.getAllOrders();
  }


  public getAllOrders() {
    this.ordersService.getAllOrders().subscribe(
      (success: any) => {
        this.orders = success;

        const calculatorQuote = success.reduce((accumulator, currentValue) => {

          if (!accumulator[currentValue.quote]) {
            accumulator[currentValue.quote] = {
              buy: 0,
              sell: 0,
              quote: currentValue.quote
            };
          }

          if (currentValue.side === 1) {
            accumulator[currentValue.quote]['buy'] += currentValue.quantity;
          } else {
            accumulator[currentValue.quote]['sell'] += currentValue.quantity;
          }

          return accumulator;
        }, {});

        const quotes = Object.values(calculatorQuote).filter(order => order['buy'] !== order['sell']).map(order => order['quote']);
        this.uniqueQuotes = Array.from(new Set(quotes));
      }
    );
  }

  public closePositions(quote) {
    const price = this.webSocketService.cacheQuotes[quote];
    this.ordersService.closePositions(quote, price).subscribe(
      () => {
        this.walletService.eventWallet.emit(true);
        this.getAllOrders();
        this.snackbarService.add({
          msg: `Ativo ${quote} encerrado com sucesso.`,
          action: {
            text: 'X',
          },
        });
      }
    );
  }

}
