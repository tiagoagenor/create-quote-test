import { Component, OnInit } from '@angular/core';
import { WebSocketService } from '../web-socket.service';
import { OrdersService } from '../orders.service';
import { WalletService } from '../wallet.service';
import { SnackbarService } from 'ngx-snackbar';

@Component({
  selector: 'app-actions',
  templateUrl: './actions.component.html',
  styleUrls: ['./actions.component.css']
})
export class ActionsComponent implements OnInit {

  public quotes: any = {};
  public quantity = {};
  public nameQuotes = ['GGBR4', 'ABEV3', 'ELET3', 'CVCB3', 'CIEL3', 'LREN3', 'B3SA3', 'SUZB3', 'ITSA4', 'MRFG3',
  'RAIL3', 'RADL3', 'CCRO3', 'RENT3', 'NATU3', 'BRDT3', 'ITUB4', 'ELET6', 'BRFS3', 'PCAR4', 'CMIG4', 'SBSP3', 'CSNA3',
  'AZUL4', 'GOAU4', 'GNDI3', 'BBDC4', 'PETR3', 'UGPA3', 'BTOW3', 'VALE3', 'KROT3', 'JBSS3', 'MGLU3', 'BBAS3', 'BBSE3',
  'GOLL4', 'VVAR3', 'PETR4', 'IRBR3'];
  constructor(
    private webSocketService: WebSocketService,
    private ordersService: OrdersService,
    private walletService: WalletService,
    private snackbarService: SnackbarService
  ) { }

  ngOnInit() {
    this.webSocketService.quotes.subscribe((dataQuote) => {
      const quoteToJson = JSON.parse(dataQuote);
      const [quote] = Object.entries(quoteToJson);
      const [nameQuote, valueQuote] = quote;
      this.quotes[nameQuote] = valueQuote;
    });
  }

  buyQuote(quote, quantity, price) {
    this.ordersService.buy(quote, Number(quantity), price).subscribe(
      () => {
        this.walletService.eventWallet.emit(true);
        this.snackbarService.add({
          msg: `Compra de ativo ${quote} feito com sucesso.`,
          action: {
            text: 'X',
          },
        });
      }
    );
  }

}
