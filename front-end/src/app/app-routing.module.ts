import { FundsComponent } from './funds/funds.component';
import { WalletComponent } from './wallet/wallet.component';
import { ActionsComponent } from './actions/actions.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: '/actions', pathMatch: 'full' },
  { path: 'actions', component: ActionsComponent },
  { path: 'funds', component: FundsComponent },
  { path: 'wallet', component: WalletComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
