import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class WalletService {

  public eventWallet = new EventEmitter<any>();

  constructor(
    private http: HttpClient
  ) { }

  public addFunds(price) {
    return this.http.post(
      `${environment.server_path}/wallet/add-funds`,
      {price}
    );
  }

  public withdrawFunds(price) {
    return this.http.post(
      `${environment.server_path}/wallet/withdraw-funds`,
      {price}
    );
  }

  public getWallet() {
    return this.http.get(
      `${environment.server_path}/wallet`
    );
  }
}
