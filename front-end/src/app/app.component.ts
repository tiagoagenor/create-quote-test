import { Component } from '@angular/core';
import { WebSocketService } from './web-socket.service';
import { WalletService } from './wallet.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public title = 'front-end';

  public wallet;

  constructor(
    private webSocketService: WebSocketService,
    private walletService: WalletService
  ) {
    this.webSocketService.OnMessage((message) => {
      this.webSocketService.quotes.emit(message);
    });

    this.getWallet();

    this.walletService.eventWallet.subscribe(() => {
      this.getWallet();
    });

  }

  getWallet() {
    this.walletService.getWallet().subscribe(
      (success: any) => {
        this.wallet = success.balance;
      }
    );
  }
}
