import { Injectable, EventEmitter, Output } from '@angular/core';
import * as webSocket from 'websocket';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WebSocketService {

  public client;
  public quotes = new EventEmitter<any>();

  public cacheQuotes = {};

  constructor(
  ) {
    const W3CWebSocket = webSocket.w3cwebsocket;
    this.client = new W3CWebSocket(`${environment.socket_path}/quotes`);
    this.cacheQuotes = {};
  }

  public OnMessage(callback: Function) {
    const This = this;
    this.client.onmessage = function(e) {
      if (typeof e.data === 'string') {

        const data = JSON.parse(e.data);
        const keys = Object.keys(data);
        const quote = keys[0];
        const value = data[quote];
        This.cacheQuotes[quote] = value;

        callback(e.data);

        // if (!this.cacheQuotes[quote]) {
        //   this.cacheQuotes[quote] = 0;
        // }
        // this.cacheQuotes[quote] = value;
        // console.log(this.cacheQuotes);
      }
    };
  }
}
