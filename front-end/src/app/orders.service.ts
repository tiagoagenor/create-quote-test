import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  constructor(
    private http: HttpClient
  ) { }

  public buy(quote, quantity, price) {
    return this.http.post(
      `${environment.server_path}/orders/buy`,
      {
        quote,
        quantity,
        price
      }
    );
  }

  public getAllOrders() {
    return this.http.get(`${environment.server_path}/orders`);
  }

  public closePositions(quote, price) {
    return this.http.post(`${environment.server_path}/orders/close-positions`, {quote, price});
  }
}
