using System.Collections.Generic;
using Domains.GatewaysInterfaces;
using wallet.Domains.Models;
using System.IO;
using System;
using System.Text;
using Newtonsoft.Json.Linq;
using System.Linq;
using wallet.Domains.Models.Enums;

namespace Gateways.FileSystem
{
  public class OrdersGateways : IOrders
  {
    private readonly string currentDirectory = Directory.GetCurrentDirectory();
    private string pathOrders;
    public OrdersGateways()
    {
      this.pathOrders = $"{this.currentDirectory}/Files/orders.json";

      if (!File.Exists(this.pathOrders))
      {
        File.Create(this.pathOrders);
      }
    }

    public string CreateOrder(OrdersModel ordersModel)
    {
      ordersModel.id = Guid.NewGuid().ToString();
      string json = Newtonsoft.Json.JsonConvert.SerializeObject(ordersModel);
      using (StreamWriter sw = File.AppendText(this.pathOrders))
      {
        sw.WriteLine(json);
      }

      return this.AllOrders();
    }

    public string AllOrders()
    {
      string[] getOrders = File.ReadAllLines(this.pathOrders, Encoding.UTF8);

      return $"[{String.Join(",", getOrders)}]";
    }

    public BuyAndSellOrderQuantityModels BuyAndSellOrderQuantity(OrdersClosePositionsModel ordersClosePositionsModel)
    {

      BuyAndSellOrderQuantityModels buyAndSellOrderQuantityModels = new BuyAndSellOrderQuantityModels();

      buyAndSellOrderQuantityModels.buy = this.TotalQuantity(ordersClosePositionsModel.quote, (int)SideEnum.buy);
      buyAndSellOrderQuantityModels.sell = this.TotalQuantity(ordersClosePositionsModel.quote, (int)SideEnum.sell); ;
      return buyAndSellOrderQuantityModels;
    }

    public int TotalQuantity(string quote, int side)
    {

      string[] orders = File.ReadAllLinesAsync(this.pathOrders, Encoding.UTF8).Result;

      int quantity = 0;

      if (orders.Length > 0)
      {
        foreach (var order in orders)
        {
          JToken token = JObject.Parse(order);

          if (side == (int)token.SelectToken("side") && (string)token.SelectToken("quote") == quote)
          {
            quantity += (int)token.SelectToken("quantity");
          }
        }
      }

      return quantity;
    }

  }

}