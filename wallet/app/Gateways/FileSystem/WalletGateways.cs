using System.Collections.Generic;
using Domains.GatewaysInterfaces;
using wallet.Domains.Models;
using System.IO;
using System;
using System.Text;
using Newtonsoft.Json.Linq;
using wallet.Domains.Models.Enums;

namespace Gateways.FileSystem
{
  public class WalletGateways : IWallet
  {
    private readonly string currentDirectory = Directory.GetCurrentDirectory();
    private string pathWallet;

    public string fileWallet = "/Files/wallet.json";

    public WalletGateways(string fileWallet = null)
    {
      if (fileWallet != null)
      {
        this.fileWallet = fileWallet;
      }

      this.pathWallet = $"{this.currentDirectory}{this.fileWallet}";

      this.CreateFile();
    }

    public void CreateFile()
    {
      if (!File.Exists(this.pathWallet))
      {
        File.Create(this.pathWallet);
      }
    }

    public TotalBalanceModel MoveWallet(WalletModels walletModels)
    {
      string json = Newtonsoft.Json.JsonConvert.SerializeObject(walletModels);

      using (StreamWriter sw = File.AppendText(this.pathWallet))
      {
        sw.WriteLine(json);
      }

      return this.TotalBalance();
    }

    public TotalBalanceModel TotalBalance()
    {
      string[] getWallet = File.ReadAllLines(this.pathWallet, Encoding.UTF8);
      double balance = 0.0;

      foreach (var itemWallet in getWallet)
      {
        JToken token = JObject.Parse(itemWallet);
        double value = (int)token.SelectToken("price");

        if ((int)token.SelectToken("side") == (int)SideEnum.buy)
        {
          balance += value;
        }
        else
        {
          balance -= value;
        }
      }

      TotalBalanceModel totalBalanceModel = new TotalBalanceModel();
      totalBalanceModel.balance = balance;

      return totalBalanceModel;
    }

  }
}