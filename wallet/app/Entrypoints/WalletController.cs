using System;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using wallet.Domains.Models;
using wallet.Domains.UseCases;

namespace wallet.Entrypoints
{
  [Route("[controller]")]
  public class WalletController : Controller
  {

    private readonly WalletAddFundsUseCases _walletAddFundsUseCases;
    private readonly WalletWithdrawFundsUseCases _walletWithdrawFundsUseCases;
    private readonly WalletGetWalletUseCases _walletGetWalletUseCases;
    public WalletController(
      WalletAddFundsUseCases walletAddFundsUseCases,
      WalletWithdrawFundsUseCases walletWithdrawFundsUseCases,
      WalletGetWalletUseCases walletGetWalletUseCases
    )
    {
      this._walletAddFundsUseCases = walletAddFundsUseCases;
      this._walletWithdrawFundsUseCases = walletWithdrawFundsUseCases;
      this._walletGetWalletUseCases = walletGetWalletUseCases;
    }

    [HttpPost("add-funds")]
    public IActionResult AddFunds([FromBody] WalletFundsModels walletFundsModels)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      return Ok(this._walletAddFundsUseCases.AddFunds(walletFundsModels));
    }

    [HttpPost("withdraw-funds")]
    public IActionResult WithdrawFunds([FromBody] WalletFundsModels walletFundsModels)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      return Ok(this._walletWithdrawFundsUseCases.WithdrawFunds(walletFundsModels));
    }

    [HttpGet]
    public IActionResult GetWallet()
    {
      return Ok(this._walletGetWalletUseCases.GetWallet());
    }

    [HttpGet("history")]
    public IActionResult History()
    {
      return Ok();
    }

  }
}
