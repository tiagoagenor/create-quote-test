using Microsoft.AspNetCore.Mvc;
using System;
using wallet.Domains.Models;
using wallet.Domains.UseCases;

namespace wallet.Entrypoints
{
  [Route("[controller]")]
  public class OrdersController : Controller
  {
    private readonly OrdersClosePositionsUseCases _ordersClosePositionsUseCases;
    private readonly OrdersBuyUseCases _ordersBuyUseCases;
    private readonly OrdersGetUseCases _ordersGetUseCases;
    public OrdersController(
      OrdersClosePositionsUseCases ordersClosePositionsUseCases,
      OrdersBuyUseCases ordersBuyUseCases,
      OrdersGetUseCases ordersGetUseCases
    )
    {
      this._ordersClosePositionsUseCases = ordersClosePositionsUseCases;
      this._ordersBuyUseCases = ordersBuyUseCases;
      this._ordersGetUseCases = ordersGetUseCases;
    }

    [HttpPost("buy")]
    public IActionResult Buy([FromBody] OrdersModel ordersBuyModel)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      return Ok(this._ordersBuyUseCases.Buy(ordersBuyModel));
    }

    [HttpPost("close-positions")]
    public IActionResult ClosePositions([FromBody] OrdersClosePositionsModel ordersClosePositionsModel)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }
      
      return Ok(this._ordersClosePositionsUseCases.ClosePositions(ordersClosePositionsModel));
    }

    [HttpGet]
    public IActionResult Get()
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }
      
      return Ok(this._ordersGetUseCases.Get());
    }

    [HttpGet("history")]
    public IActionResult History()
    {
      return Ok();
    }

  }
}
