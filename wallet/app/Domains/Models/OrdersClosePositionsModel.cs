using System.ComponentModel.DataAnnotations;

namespace wallet.Domains.Models
{
  public class OrdersClosePositionsModel
  {
    [Required]
    public string quote { get; set; }

    [Required]
    [Range(1, 999999999)]
    public double price { get; set; }

  }
}
