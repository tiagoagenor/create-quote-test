using System.ComponentModel.DataAnnotations;

namespace wallet.Domains.Models
{
  public class BuyAndSellTotalOrderModels
  {
    public double priceBuy { get; set; }
    public double priceSell { get; set; }

  }
}
