
namespace wallet.Domains.Models.Enums
{
  public enum WalletTypeEnum {
    addFunds = 1,
    withdrawFunds = 2,
    orderPurchase = 3,
    sellOrder = 4
  }

}