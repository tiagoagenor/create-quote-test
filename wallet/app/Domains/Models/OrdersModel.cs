using System.ComponentModel.DataAnnotations;

namespace wallet.Domains.Models
{
  public class OrdersModel{

    public string id { get; set; }
    
    [Required]
    public string quote { get; set; }
    public int side { get; set; }

    [Required]
    [Range(1, 999999999)]
    public int quantity { get; set; }

    [Required]
    [Range(1, 999999999)]
    public double price  { get; set; }
  }
}
