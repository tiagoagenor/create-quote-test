using System.ComponentModel.DataAnnotations;

namespace wallet.Domains.Models
{
  public class BuyAndSellOrderQuantityModels
  {
    public int buy { get; set; }
    public int sell { get; set; }

  }
}
