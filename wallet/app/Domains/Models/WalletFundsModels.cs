using System.ComponentModel.DataAnnotations;

namespace wallet.Domains.Models
{
  public class WalletFundsModels{
    [Required]
    [Range(1, 999999999)]
    public double price { get; set; }
  }
}
