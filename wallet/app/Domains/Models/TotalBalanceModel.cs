using System.ComponentModel.DataAnnotations;

namespace wallet.Domains.Models
{
  public class TotalBalanceModel{
    public double balance { get; set; }
  }
}
