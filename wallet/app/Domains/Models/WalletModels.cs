using System.ComponentModel.DataAnnotations;

namespace wallet.Domains.Models
{
  public class WalletModels
  {
    public string id { get; set; }
    public double price { get; set; }
    public int side { get; set; }
    public int type { get; set; }
  }
}
