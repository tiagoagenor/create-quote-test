using Domains.GatewaysInterfaces;
using Gateways.FileSystem;
using System;
using wallet.Domains.Models;
using wallet.Domains.Models.Enums;

namespace wallet.Domains.UseCases
{
  public class OrdersBuyUseCases
  {

    private readonly IOrders _ordersGateways;
    private readonly IWallet _wallet;
    public OrdersBuyUseCases(
      IOrders ordersGateways,
      IWallet wallet
    )
    {
      this._ordersGateways = ordersGateways;
      this._wallet = wallet;
    }
    public string Buy(OrdersModel ordersModel)
    {
      ordersModel.side = (int)SideEnum.buy;

      WalletModels walletModels = this.MountMoveWallet(ordersModel);
      this._wallet.MoveWallet(walletModels);

      return this._ordersGateways.CreateOrder(ordersModel);
    }

    public WalletModels MountMoveWallet(OrdersModel ordersModel)
    {
      WalletModels walletModels = new WalletModels();
      walletModels.id = Guid.NewGuid().ToString();
      walletModels.price = ordersModel.price * ordersModel.quantity;
      walletModels.side = (int)SideEnum.sell;
      walletModels.type = (int)WalletTypeEnum.withdrawFunds;
      return walletModels;
    }
  }
}
