using Domains.GatewaysInterfaces;
using Gateways.FileSystem;
using System;
using wallet.Domains.Models;
using wallet.Domains.Models.Enums;

namespace wallet.Domains.UseCases
{
  public class OrdersGetUseCases
  {
    private readonly IOrders _ordersGateways;
    private readonly IWallet _wallet;

    public OrdersGetUseCases(
      IOrders ordersGateways,
      IWallet wallet
    )
    {
      this._ordersGateways = ordersGateways;
      this._wallet = wallet;
    }
    public string Get()
    {
      return this._ordersGateways.AllOrders();
    }
  }
}
