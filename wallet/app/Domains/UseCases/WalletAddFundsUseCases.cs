using Domains.GatewaysInterfaces;
using System;
using wallet.Domains.Models;
using wallet.Domains.Models.Enums;

namespace wallet.Domains.UseCases
{
  public class WalletAddFundsUseCases
  {
    private readonly IWallet _wallet;

    public WalletAddFundsUseCases(
      IWallet wallet
    )
    {
      this._wallet = wallet;
    }

    public TotalBalanceModel AddFunds(WalletFundsModels walletFundsModels)
    {
      WalletModels walletModels = this.MountwalletModels(walletFundsModels);

      return this._wallet.MoveWallet(walletModels);
    }

    public WalletModels MountwalletModels(WalletFundsModels walletFundsModels)
    {
      WalletModels walletModels = new WalletModels();
      walletModels.id = Guid.NewGuid().ToString();
      walletModels.price = walletFundsModels.price;
      walletModels.side = (int)SideEnum.buy;
      walletModels.type = (int)WalletTypeEnum.addFunds;

      return walletModels;
    }
  }
}
