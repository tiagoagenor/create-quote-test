using Domains.GatewaysInterfaces;
using System;
using wallet.Domains.Models;
using wallet.Domains.Models.Enums;

namespace wallet.Domains.UseCases
{
  public class WalletGetWalletUseCases
  {
    private readonly IWallet _wallet;

    public WalletGetWalletUseCases(
      IWallet wallet
    )
    {
      this._wallet = wallet;
    }

    public TotalBalanceModel GetWallet()
    {
      return this._wallet.TotalBalance();
    }
  }
}
