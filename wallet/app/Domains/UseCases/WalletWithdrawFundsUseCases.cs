using Domains.GatewaysInterfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using wallet.Domains.Models;
using wallet.Domains.Models.Enums;

namespace wallet.Domains.UseCases
{
  public class WalletWithdrawFundsUseCases
  {

    
    private readonly IWallet _wallet;

    public WalletWithdrawFundsUseCases(
      IWallet wallet
    )
    {
      this._wallet = wallet;
    }
    
    public TotalBalanceModel WithdrawFunds(WalletFundsModels walletFundsModels)
    {
      WalletModels walletModels = this.MountwalletModels(walletFundsModels);

      return this._wallet.MoveWallet(walletModels);
    }

    public WalletModels MountwalletModels(WalletFundsModels walletFundsModels)
    {
      WalletModels walletModels = new WalletModels();
      walletModels.id = Guid.NewGuid().ToString();
      walletModels.price = walletFundsModels.price;
      walletModels.side = (int)SideEnum.sell;
      walletModels.type = (int)WalletTypeEnum.withdrawFunds;

      return walletModels;
    }
  }
}
