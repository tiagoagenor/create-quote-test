using Domains.GatewaysInterfaces;
using Gateways.FileSystem;
using System;
using wallet.Domains.Models;
using wallet.Domains.Models.Enums;

namespace wallet.Domains.UseCases
{
  public class OrdersClosePositionsUseCases
  {
    private readonly IOrders _ordersGateways;
    private readonly IWallet _wallet;

    public OrdersClosePositionsUseCases(
      IOrders ordersGateways,
      IWallet wallet
    )
    {
      this._ordersGateways = ordersGateways;
      this._wallet = wallet;
    }
    public string ClosePositions(OrdersClosePositionsModel ordersClosePositionsModel)
    {
      BuyAndSellOrderQuantityModels buyAndSellOrderQuantityModels = this._ordersGateways.BuyAndSellOrderQuantity(ordersClosePositionsModel);
      int sell = buyAndSellOrderQuantityModels.buy - buyAndSellOrderQuantityModels.sell;

      if (buyAndSellOrderQuantityModels.buy > buyAndSellOrderQuantityModels.sell)
      {
        WalletModels walletModels = this.MountMoveWallet(ordersClosePositionsModel, sell);
        this._wallet.MoveWallet(walletModels);

        OrdersModel ordersModel = this.MountSellOrder(ordersClosePositionsModel, sell);
        return this._ordersGateways.CreateOrder(ordersModel);
      }
      else
      {
        return this._ordersGateways.AllOrders();
      }
    }

    public OrdersModel MountSellOrder(OrdersClosePositionsModel ordersClosePositionsModel, int sell)
    {

      OrdersModel ordersModel = new OrdersModel();
      ordersModel.price = ordersClosePositionsModel.price;
      ordersModel.quote = ordersClosePositionsModel.quote;
      ordersModel.quantity = sell;
      ordersModel.side = (int)SideEnum.sell;

      return ordersModel;
    }

    public WalletModels MountMoveWallet(OrdersClosePositionsModel ordersClosePositionsModel, int sell)
    {
      WalletModels walletModels = new WalletModels();
      walletModels.id = Guid.NewGuid().ToString();
      walletModels.price = ordersClosePositionsModel.price * sell;
      walletModels.side = (int)SideEnum.buy;
      walletModels.type = (int)WalletTypeEnum.sellOrder;
      return walletModels;
    }
  }
}
