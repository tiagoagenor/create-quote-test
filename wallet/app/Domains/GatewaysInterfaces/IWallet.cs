using System.Collections.Generic;
using wallet.Domains.Models;

namespace Domains.GatewaysInterfaces
{
  public interface IWallet{
    TotalBalanceModel MoveWallet(WalletModels walletModels);
    TotalBalanceModel TotalBalance();
  }
}