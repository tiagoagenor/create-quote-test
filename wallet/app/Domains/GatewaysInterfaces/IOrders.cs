using System.Collections.Generic;
using wallet.Domains.Models;

namespace Domains.GatewaysInterfaces
{
  public interface IOrders{
    string CreateOrder(OrdersModel ordersModel);
    string AllOrders();
    BuyAndSellOrderQuantityModels BuyAndSellOrderQuantity(OrdersClosePositionsModel ordersClosePositionsModel);
    int TotalQuantity(string quote, int side);
  }
}