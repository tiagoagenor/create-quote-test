using System;
using Xunit;
using Gateways.FileSystem;
using System.IO;
using System.Text;
using Xunit.Sdk;
using wallet.Domains.Models;
using wallet.Domains.Models.Enums;

namespace walletTest
{
  public class WalletGatewaysTest
  {

    private readonly WalletGateways _walletGateways;

    private readonly string walletTest = "/../../../Files/wallet-test.json";
    private string _pathWallet;

    public WalletGatewaysTest()
    {
      this._pathWallet = $"{Directory.GetCurrentDirectory()}{this.walletTest}";
      this.CleanFile();

      this._walletGateways = new WalletGateways(this.walletTest);
    }

    [Fact]
    public void CreateFileExists()
    {
      Assert.True(File.Exists(this._pathWallet));
    }

    [Fact]
    public void MoveWalletInsertTemplateInWallet()
    {
        WalletModels walletModels = new WalletModels();
        this._walletGateways.MoveWallet(walletModels);

        string[] getWallet = File.ReadAllLines(this._pathWallet, Encoding.UTF8);

        Assert.Equal((int)getWallet.Length, 1);
        this.CleanFile();
    }

    [Fact]
    public void MoveWalletInsertCheckingPositiveBalance()
    {
        WalletModels walletModels = new WalletModels();
        double addValue = 150;
        walletModels.price = addValue;
        walletModels.side = (int)SideEnum.buy;

        TotalBalanceModel totalBalanceModel =  this._walletGateways.MoveWallet(walletModels);
        Assert.Equal(totalBalanceModel.balance, addValue);
        this.CleanFile();
    }

    public void MoveWalletInsertCheckingNegativeBalance()
    {
        WalletModels walletModels = new WalletModels();
        double addValue = 150;
        walletModels.price = addValue;
        walletModels.side = (int)SideEnum.sell;
        
        TotalBalanceModel totalBalanceModel =  this._walletGateways.MoveWallet(walletModels);
        Assert.Equal(totalBalanceModel.balance, addValue*(-1));
        this.CleanFile();
    }

    private void CleanFile() {
      if (File.Exists(this._pathWallet)) {
        File.WriteAllText(this._pathWallet, String.Empty);
      }
    }

  }
}
