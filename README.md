# Instalação

A instalação do sistema e bem simples.
Primeiro verifique os arquivos de configuração dos sistemas.
>./front-end/src/environments/environment.ts

>./wallet/appsettings.json

Em seguida inicie o sistema com o seguinte comando.
> sudo docker-compose up -d

# Teste de cotação

Entre na pasta *"quote"* depois execute os seguintes comandos.
> npm i

> node index.js

# Teste unitario

Segue abaixo os passos para executar os testes unitários.
 
Entre na pasta de teste unitário.
> cd ./wallet/walletTest
 
Em seguida execute o seguinte comando
> dotnet test